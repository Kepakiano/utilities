#ifndef TIME_H
#define TIME_H

#include <chrono>
#include <ctime>
#include <iostream>
#include <functional>

namespace Utilities {
namespace Time {

inline std::chrono::time_point<std::chrono::system_clock> startMeasurement()
{
    return std::chrono::system_clock::now();
}

inline double finishMeasurement(const std::chrono::time_point<std::chrono::system_clock> & start, bool verbose = true)
{
    std::chrono::time_point<std::chrono::system_clock> end = std::chrono::system_clock::now();

    const std::chrono::duration<double> elapsed_seconds = end-start;
    const std::time_t start_time = std::chrono::system_clock::to_time_t(start);
    const std::time_t end_time   = std::chrono::system_clock::to_time_t(end);
    const std::string start_time_string = std::ctime(&start_time);
    const std::string end_time_string = std::ctime(&end_time);

    if(verbose){
        std::cout << "started computation at " << start_time_string
                  << "finished computation at " << end_time_string
                  << "elapsed time: " << elapsed_seconds.count() << "s" << std::endl;
    }
    return elapsed_seconds.count();
}

inline void timeFunction(std::function<void()> func)
{
    const auto & start = Utilities::Time::startMeasurement();
    func();
    Utilities::Time::finishMeasurement(start);
}

template<class T>
inline T timeFunction(std::function<T()> f)
{
    const auto & start = Utilities::Time::startMeasurement();
    T retVal = f();
    Utilities::Time::finishMeasurement(start);
    return retVal;
}

}
}
#endif
