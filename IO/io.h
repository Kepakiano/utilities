#ifndef IO_H
#define IO_H

#include <vector>
#include <set>
#include <algorithm>
#include <cassert>
#include <iomanip>
#include <sstream>
#include <map>
#include <numeric>
#include <sys/stat.h>
#include <iostream>
#include <cmath>

namespace Utilities {
namespace IO {

template<class T>
inline bool vectorContains(const std::vector<T> & container, const T & value)
{
    return std::find(container.begin(), container.end(), value) != container.end();
}

template<class T, class V>
inline bool mapContains(const std::map<T, V> & container, const T & value)
{
    return container.find(value) != container.end();
}

template<class T, class V>
inline std::vector<V> mapValues(const std::map<T, V> & map)
{
    std::vector<V> values;
    for(const auto & p : map)
        values.push_back(p.second);
    return values;
}

template<class T>
inline int vectorIndexOf(const std::vector<T> & vec, const T & value)
{
    if(!vectorContains(vec, value))
        return -1;
    return find(vec.begin(), vec.end(), value) - vec.begin();
}

template<typename T>
inline T vectorSum(const std::vector<T> & vec)
{
    return std::accumulate(vec.begin(), vec.end(), T());
}

template<typename T>
inline T vectorEuclideanLength(const std::vector<T> & vec)
{
    return sqrt(std::accumulate(vec.begin(), vec.end(), T(), [](T d1, T d2){return d1 + d2*d2;}));
}

template<typename T>
inline T maxElement2D(const std::vector<std::vector<T>> & vec, const bool useAbsoluteValue = false)
{
    assert(vec.size() > 0 && vec.begin()->size() > 0);
    T maxElem = vec.at(0).at(0);
    for(const auto & v : vec){
        if(useAbsoluteValue)
            maxElem = std::max(maxElem, *std::max_element(v.begin(), v.end()), [](const T & t1,const T & t2){return std::abs(t1) < std::abs(t2);});
        else
            maxElem = std::max(maxElem, *std::max_element(v.begin(), v.end()));
    }
    return maxElem;
}

template<typename T>
inline void prettyPrint(const std::vector<std::vector<T>> & vec, const size_t precision = 2)
{
    const T maxAbsElem = maxElement2D(vec, true);
    const int width = std::log10((int)maxAbsElem) + 1 + precision + (precision > 0 ? 1 : 0);

    std::cout << std::setprecision(precision) << std::fixed;
    for(const auto & v : vec){
        for(const auto & t : v){
            std::cout << std::setw(width) << t << " ";
        }
        std::cout << std::endl;
    }
}

inline void prettyPrint(const std::vector<std::vector<std::string>> & vec)
{
    const std::string longestString = std::accumulate(vec.begin(), vec.end(), std::string(""), [](std::string longest, std::vector<std::string> v)
                                                    { const std::string newString = *std::max_element(v.begin(), v.end(), [](const std::string& str1,const std::string& str2){return str1.size() < str2.size();});
                                                      return longest.size() > newString.size() ? longest : newString;
                                                    });
    const int width = longestString.size();

    for(const auto & v : vec){
        for(const auto & t : v){
            std::cout << std::setw(width) << t << " ";
        }
        std::cout << std::endl;
    }
}

inline void prettyPrint(const std::vector<std::vector<bool>> & vec)
{

    for(const auto & v : vec){
        for(const auto & t : v){
            std::cout << t << " ";
        }
        std::cout << std::endl;
    }
}

inline bool fileExists (const std::string& name) {
  struct stat buffer;
  return (stat (name.c_str(), &buffer) == 0);
}

template<class T>
inline std::vector<std::vector<T>> transpose(const std::vector<std::vector<T>> & m)
{
    const size_t rows = m.size();
    const size_t cols = std::max_element(m.begin(), m.end(), [](const std::vector<T> & v1, const std::vector<T> & v2){return v1.size() < v2.size();})->size();
    std::vector<std::vector<T>> res(cols, std::vector<T>(rows));
    for(size_t i = 0; i < m.size(); i++){
        for(size_t j = 0; j < m.at(i).size(); j++){
            res.at(j).at(i) = m.at(i).at(j);
        }
    }
    return res;
}


inline std::vector<std::string> splitString(const std::string &s, char delim) {
    std::vector<std::string> elems;
    std::stringstream ss;
    ss.str(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}

template <typename T>
std::vector<T> flatten(const std::vector<std::vector<T>>& v) {
    std::size_t total_size = 0;
    for (const auto& sub : v)
        total_size += sub.size();
    std::vector<T> result;
    result.reserve(total_size);
    for (const auto& sub : v)
        result.insert(result.end(), sub.begin(), sub.end());
    return result;
}

}
}

#endif // IO_H
