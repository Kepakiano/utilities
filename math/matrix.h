#ifndef MATRIX_H
#define MATRIX_H

#include <cassert>
#include <vector>

template <typename T> class Matrix {

  std::vector<T> _matrix;
  const int _rows;
  const int _cols;

public:
  Matrix(int rows, int cols, T def) : _matrix(rows * cols, def), _rows(rows), _cols(cols) {}
  Matrix(int rows, int cols) : Matrix(rows, cols, T()) {}

  class Row {
  private:
    Matrix<T> &_m;
    const int _row;

  public:
    Row(Matrix<T> &m, int row) : _m(m), _row(row) {}

    decltype(auto) operator[](int col) {
      assert(col >= 0);
      assert(col < _m.cols());
      return _m._matrix[_row * _m.cols() + col];
    }
  };
  friend class Row;

  Row operator[](int row) {
    assert(row >= 0);
    assert(row < _rows);
    return Row(*this, row);
  }

  int rows() const { return _rows; }
  int cols() const { return _cols; }
};

#endif // MATRIX_H
