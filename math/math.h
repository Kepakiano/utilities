#ifndef UTILITIES_H
#define UTILITIES_H

#include <vector>
#include <deque>
#include <chrono>
#include <random>
#include <algorithm>
#include <cfloat>
#include <iostream>
#include <functional>
#include <stdexcept>

namespace Utilities {
namespace Math {


template<typename T>
static T mean(const std::vector<T> & values)
{
    return accumulate(values.begin(), values.end(), T(), std::plus<T>())/values.size();
}

template<typename T>
static double standardDeviation(const std::vector<T> & values, T mean)
{
    double sigma = 0.0;
    for(const T & d : values)
        sigma += pow(d - mean, 2);
    sigma /= values.size();
    sigma = sqrt(sigma);
    return sigma;
}

template<typename T>
static std::pair<T, double> meanAndDeviation(const std::vector<T> & values)
{
    T meanValue = mean(values);
    double stdev = standardDeviation(values, meanValue);
    return std::make_pair(meanValue, stdev);
}

template<class T, class V>
std::vector<std::vector<double>> calculateDistanceMatrix(const std::vector<T> & x,
                                                         const std::vector<T> & y,
                                                         V distance,
                                                         const std::vector<std::vector<bool>> & warping_path_matrix)
{
    std::vector<std::vector<double>> distanceMatrix = std::vector<std::vector<double>>(y.size(), std::vector<double>(x.size(), std::numeric_limits<double>::max())); // 2D matrix, all elements are a double's maximum value
    distanceMatrix[0][0] = 0;

    for (size_t j = 0; j < y.size();++j){
      for(size_t i = 0; i < x.size();++i){
            if(!warping_path_matrix[i][j])
              continue;
            double cost = pow(distance(x[i],y[j]), 2.0);
            if(i == 0 && j == 0)
                distanceMatrix[j][i] = cost;
            else if (i == 0)
                distanceMatrix[j][i] = cost + distanceMatrix[j-1][i];
            else if (j == 0)
                distanceMatrix[j][i] = cost + distanceMatrix[j][i-1];
            else
                distanceMatrix[j][i] = cost + std::min(distanceMatrix[j-1][i], std::min(distanceMatrix[j][i-1],distanceMatrix[j-1][i-1]));
      }
    }
    return distanceMatrix;
}

template<class T, class V>
std::vector<std::vector<double>> calculateDistanceMatrix(const std::vector<T> & x,
                                                         const std::vector<T> & y,
                                                         V distance,
                                                         const std::vector<std::pair<int,int>> & warping_path_matrix)
{
    std::vector<std::vector<double>> distanceMatrix = std::vector<std::vector<double>>(y.size(), std::vector<double>(x.size(), std::numeric_limits<double>::max())); // 2D matrix, all elements are a double's maximum value
    distanceMatrix[0][0] = 0;

    for(const std::pair<int, int> & p : warping_path_matrix){
      int i = p.first;
      int j = p.second;

      double cost = pow(distance(x[i],y[j]), 2.0);
      if(i == 0 && j == 0)
        distanceMatrix[j][i] = cost;
      else if (i == 0)
        distanceMatrix[j][i] = cost + distanceMatrix[j-1][i];
      else if (j == 0)
        distanceMatrix[j][i] = cost + distanceMatrix[j][i-1];
      else
        distanceMatrix[j][i] = cost + std::min(distanceMatrix[j-1][i], std::min(distanceMatrix[j][i-1],distanceMatrix[j-1][i-1]));
    }
    return distanceMatrix;
}

template<class T, class V>
std::vector<std::vector<double>> calculateDistanceMatrix(const std::vector<T> & x,
                                                         const std::vector<T> & y,
                                                         V distance)
{
  return calculateDistanceMatrix(x,y,distance, std::vector<std::vector<bool>>(x.size(), std::vector<bool>(y.size(), true)));

}

template<class T, class V>
double distanceByDynamicTimeWarping (const std::vector<T> & x, const std::vector<T>  & y, V distance ,
                                     const std::vector<std::vector<bool>> & warping_path_matrix)
{
    const std::vector<std::vector<double>> & distanceMatrix = calculateDistanceMatrix(x,y,distance, warping_path_matrix);
    return distanceMatrix[y.size()-1][x.size()-1];
}

template<class T>
inline double distanceByDynamicTimeWarping(const std::vector<T> & x, const std::vector<T>  & y)
{
    return distanceByDynamicTimeWarping(x,y, std::minus<T>(), std::vector<std::vector<bool>>(x.size(), std::vector<bool>(y.size(), true)));
}

template<class T, class V>
inline double distanceByDynamicTimeWarping(const std::vector<T> & x,
                                           const std::vector<T> & y,
                                           V distance)
{
    return distanceByDynamicTimeWarping(x,
                                        y,
                                        distance,
                                        std::vector<std::vector<bool>>(x.size(), std::vector<bool>(y.size(), true)));
}

template<class T>
inline std::vector<std::pair<size_t, size_t>> dynamicWarpingPath(const std::vector<T> & x,
                                                                 const std::vector<T> & y,
                                                                 const std::vector<std::vector<double>> & distanceMatrix)
{
    int i_x = x.size() - 1;
    int i_y = y.size() - 1;

    std::vector<std::pair<size_t, size_t>> path = {{i_x, i_y}};
    while(i_x > 0 || i_y > 0){
        double distanceXY = (i_x > 0 && i_y > 0 ? distanceMatrix[i_y-1][i_x-1] : std::numeric_limits<double>::max());
        double distanceY = (i_y > 0 ? distanceMatrix[i_y-1][i_x] : std::numeric_limits<double>::max());
        double distanceX = (i_x > 0 ? distanceMatrix[i_y][i_x-1] : std::numeric_limits<double>::max());

        if(std::min(distanceXY, std::min(distanceX, distanceY)) == std::numeric_limits<double>::max())
          throw std::logic_error("There is a whole in the warping matrix at (" + std::to_string(i_x) + ", " + std::to_string(i_y) + ").");

        if(distanceXY <= distanceX && distanceXY <= distanceY){
            i_x--;
            i_y--;
        }
        else if(distanceX <= distanceY && distanceX <= distanceXY){
            i_x--;
        }
        else if(distanceY < distanceX && distanceY <= distanceXY){
            i_y--;
        }
        else{
            throw std::logic_error("Some bad programmer made a mistake. This should not happen.");
        }
        path.push_back({i_x,i_y});
    }

    std::reverse(path.begin(), path.end());

    return path;
}

template<class T, class V>
inline std::vector<std::pair<size_t, size_t>> dynamicWarpingPath(const std::vector<T> & x,
                                                                 const std::vector<T> & y,
                                                                 V distance,
                                                                 const std::vector<std::vector<bool>> & warping_path_matrix)
{
    const std::vector<std::vector<double>> & distanceMatrix = calculateDistanceMatrix(x,y,distance, warping_path_matrix);
    return dynamicWarpingPath(x,y,distanceMatrix);
}

template<class T, class V>
inline std::vector<std::pair<size_t, size_t>> dynamicWarpingPath(const std::vector<T> & x,
                                                                 const std::vector<T> & y,
                                                                 V distance,
                                                                 const std::vector<std::pair<int,int>> & warping_path_matrix)
{
    const std::vector<std::vector<double>> & distanceMatrix = calculateDistanceMatrix(x,y,distance, warping_path_matrix);
    return dynamicWarpingPath(x,y,distanceMatrix);
}

template<class T, class V>
inline std::vector<std::pair<size_t, size_t>> dynamicWarpingPath(const std::vector<T> & x,
                                                                 const std::vector<T> & y,
                                                                 V distance)
{
    const std::vector<std::vector<double>> & distanceMatrix = calculateDistanceMatrix(x,y,distance, std::vector<std::vector<bool>>(x.size(), std::vector<bool>(y.size(), true)));
    return dynamicWarpingPath(x,y,distanceMatrix);
}


template<class T>
inline std::vector<std::pair<size_t, size_t>> dynamicWarpingPath(const std::vector<T> & x,
                                                                 const std::vector<T> & y)
{
  return dynamicWarpingPath(x,y,std::minus<double>(), std::vector<std::vector<bool>>(x.size(), std::vector<bool>(y.size(), true)));
}

inline size_t getRandomNumberBetween(size_t min, size_t max)
{
    long int seed = std::chrono::system_clock::now().time_since_epoch().count();
    static std::minstd_rand RNG(seed);

    if(max == min)
        return min;
    return min + RNG() % (max - min);
}

inline double getRandomDoubleBetween(double min, double max)
{
  long int seed = std::chrono::system_clock::now().time_since_epoch().count();
  double lower_bound = min;
  double upper_bound = max;
  std::uniform_real_distribution<double> unif(lower_bound,upper_bound);
  std::default_random_engine re(seed);
  return unif(re);
}

template<typename T>
std::vector<T> resample(const std::vector<T> & timeseries, const int l)
{
    std::vector<T> resampled(l, T());
    const double step =  std::nextafter( (timeseries.size()-1)/double(l-1), DBL_MIN); // Prevents rounding errors
    for(size_t i = 0; i < l; i++){
        const double decimalIndexStart = i*step;
        const size_t indexStart = floor(decimalIndexStart);
        const size_t indexEnd = ceil(decimalIndexStart);

        const double weightRight = decimalIndexStart - indexStart;
        const double weightLeft = 1.0 - weightRight;
        const double valueLeft = timeseries.at(indexStart);
        const double valueRight = timeseries.at(indexEnd);

        resampled.at(i) = valueLeft*weightLeft+valueRight*weightRight;
    }

    return resampled;
}

template<typename T>
std::vector<T> resample(const std::vector<T> &timeseries,
                        const int current_samplerate,
                        const int target_samplerate)
{
  const double timestep = std::nextafter(current_samplerate/ (double)target_samplerate, DBL_MIN);

  std::vector<T> resampled;
  for(size_t i = 0; ; ++i){

    const double decimalIndexStart = i*timestep;
    const size_t indexStart = floor(decimalIndexStart);
    const size_t indexEnd = ceil(decimalIndexStart);

    if(indexEnd >= timeseries.size())
      break;

    const double weightRight = decimalIndexStart - indexStart;
    const double weightLeft = 1.0 - weightRight;
    const double valueLeft = timeseries.at(indexStart);
    const double valueRight = timeseries.at(indexEnd);

    resampled.push_back(valueLeft*weightLeft+valueRight*weightRight);
  }
  return resampled;
}

template<typename T>
inline std::vector<T> applyKernel(const std::vector<T> & v, const std::vector<T> & kernel)
{
    std::vector<T> filtered(v.size());
    const int kernelSize = kernel.size();
    for(int i = 0; i < (int)filtered.size(); i++){
        std::vector<T> values;
        for(int j = i - kernelSize/2; j <= i + kernelSize/2; j++){
            values.push_back(v.at(std::min((int)v.size()-1, std::max(0, j))));
        }
        double filteredValue = 0.0;
        for(int j = 0; j < kernelSize; j++){
            filteredValue += values.at(j)*kernel.at(j);
        }
        filtered.at(i) = filteredValue;
    }
    return filtered;
}

template<typename T>
inline std::vector<T> createGaussianKernel1D(const int kernelSize, const double sigma = 1.0)
{
    double r, s = 2.0 * sigma * sigma;

    // sum is for normalization
    double sum = 0.0;

    std::vector<T> kernel(kernelSize, 0.0);

    // generate 5x5 kernel
    for (int x = -kernelSize/2; x <= kernelSize/2; x++)
    {
        r = sqrt(x*x);
        kernel.at(x + kernelSize/2) = (exp(-(r*r)/s))/(M_PI * s);
        sum += kernel.at(x + kernelSize/2);
    }

    // normalize the Kernel
    for(auto & value : kernel)
        value /= sum;
    return kernel;
}

template<typename T>
std::vector<T> movingAverage(const std::vector<T> & timeseries, const int windowSize)
{
    std::vector<T> averaged;

    T currAverage = T();
    size_t currAverageSize = 0;

    // Fill the moving average with values for half the window size
    for(int i = 0; i < windowSize/2; i++){
        currAverage += timeseries.at(i);
        currAverageSize++;
    }

    for(int i = 0; i < (int)timeseries.size(); i++){
        // Whenever the window is not full, add another value
        if(i+windowSize/2 < (int)timeseries.size()){
            currAverage += timeseries.at(i+windowSize/2);
            currAverageSize++;
        }

        averaged.push_back(currAverage/currAverageSize);

        // If the window has left the left side of the timeseries, always remove a value
        if(i - windowSize/2 >= 0){
            currAverage -= timeseries.at(i-windowSize/2);
            currAverageSize--;
        }
    }

    return averaged;
}

template<typename T>
T min_element(const std::vector<T> & vec)
{
    return *std::min_element(vec.begin(), vec.end());
}

template<typename T>
std::vector<T> derivative(const std::vector<T> & vec)
{
    std::vector<T> d(vec.size(), T());
    const int vsize = vec.size();
    for(int i = 0; i < vsize; i++){
        const T & x0 = vec.at(std::max(i-1, 0));
        const T & x1 = vec.at(std::min(i+1, vsize-1));
        d.at(i) = x1 - x0;
    }
    return d;
}

template<typename T>
inline std::vector<double> normalized(const std::vector<T> & timeseries)
{
    const double sum = std::accumulate(timeseries.begin(), timeseries.end(), 0.0);
    std::vector<double> normalizedTimeseries;
    for(T v : timeseries)
        normalizedTimeseries.push_back(v/sum);

    return normalizedTimeseries;
}

template<typename T>
inline std::vector<T> z_normalized(const std::vector<T> & timeseries, double mean, double sigma)
{
    if(sigma == 0.0)
        sigma = 1.0;

    std::vector<T> normalizedTimeseries;
    for(T v : timeseries)
        normalizedTimeseries.push_back((v-mean)/sigma);

    return normalizedTimeseries;
}

template<typename T>
inline std::vector<T> z_normalized(const std::vector<T> & timeseries)
{
    auto meanAndDev = meanAndDeviation(timeseries);
    return z_normalized(timeseries, meanAndDev.first, meanAndDev.second);
}

template<typename T>
inline T percentile(std::vector<T> vec, double percent)
{
    if(vec.size() == 0)
        return T();
    auto nth = vec.begin() + (percent*vec.size())/100;
    std::nth_element(vec.begin(), nth, vec.end());
    return *nth;
}

template<typename T>
inline std::pair<T,T> getQ1Q3(const std::vector<T> & vec)
{
    T Q1 = percentile(vec, 25);
    T Q3 = percentile(vec, 75);
    return std::make_pair(Q1, Q3);
}

template<typename T>
inline T getIQR(const std::vector<T> & vec)
{
    T Q1 = percentile(vec, 25);
    T Q3 = percentile(vec, 75);
    return Q3 - Q1;
}

template<typename T>
inline T median(const std::vector<T> & vec)
{
    auto sorted_vec = vec;
    return percentile(sorted_vec, 50);
}


template<typename T>
inline std::vector<T> outliersIQR(std::vector<T> vec)
{
    const auto & Q1Q3 = getQ1Q3(vec);
    const T Q1 = Q1Q3.first;
    const T Q3 = Q1Q3.second;
    const T IQR = Q3 - Q1;

    auto it_outliers = std::partition(vec.begin(), vec.end(), [&Q1, &Q3, &IQR](T v){
        return v >= Q1 - 1.5 * IQR && v <= Q3 + 1.5 * IQR;
    });

    return std::vector<T>(it_outliers, vec.end());
}


}
}


#endif // UTILITIES_H

